use std::io::Read;
use std::net::{TcpStream, TcpListener};
use warp::Filter;

fn read_u32(mut stream : &TcpStream) -> std::io::Result<u32> 
{
    let mut buf = [0; 4];
    match stream.read(&mut buf) 
    {
        Ok(_value) => Ok((buf[0] as u32) | ((buf[1] as u32) << 8) | ((buf[2] as u32) << 16) | ((buf[3] as u32) << 24)),
        Err(e) => Err(e),
    }
}

fn read_u16(mut stream : &TcpStream) -> std::io::Result<u16> 
{
    let mut buf = [0; 2];
    match stream.read(&mut buf) {
        Ok(_value) =>  Ok((buf[4] as u16) | ((buf[5] as u16) << 8)),
        Err(e) => Err(e),
    }
}

fn handle_client(mut stream : TcpStream)
{
    let packet_id = read_u32(&stream).unwrap();
    let packet_size = read_u16(&stream).unwrap();
    let packet_type= read_u16(&stream).unwrap();

    let mut packet_data = Vec::with_capacity(packet_size.into());

    let mut temp_buf = [0; 4096];
    let mut total = 0usize;
    loop
    {
        let bytes_read = match stream.read(&mut temp_buf)
        {
            Ok(r) => 
            {
                r
            },
            Err(_e) => 
            {
                println!("Error");
                0
            }
        };
        println!("bytes read: {}", bytes_read);


        if bytes_read == 0 
        {
            break;
        }

        packet_data.extend_from_slice(&temp_buf[..bytes_read]);
        total += bytes_read;

        if total == packet_size as usize
        {
            break;
        }
    }

    let s = match std::str::from_utf8(&packet_data) {
        Ok(r) => r,
        Err(e) => "Error"
    };
    println!("{}", s);
    println!("{}", packet_size);
}

#[tokio::main]
async fn main() -> std::io::Result<()>
{
    //let listener = TcpListener::bind("0.0.0.0:6969").unwrap();

    //for stream in  listener.incoming() 
    //{
    //    handle_client(stream?);
    //}


    let index = warp::get()
        .and(warp::path::end())
        .and(warp::fs::file("./index.html"));

    let routes = index;

    warp::serve(routes)
        .run(([127, 0, 0, 1], 3030))
        .await;
    Ok(())
}
